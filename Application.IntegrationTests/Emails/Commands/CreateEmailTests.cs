﻿using Application.Emails.Commands.CreateEmail;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.IntegrationTests.Emails.Commands
{
    using static Testing;

    public class CreateEmailTests : TestBase
    {
        [Test]
        public async Task ShouldReturnGuidOfNewEmail()
        {
            var createEmailCommand = new CreateEmailCommand()
            {
                Subject = "Test subject",
                Message = "Test Message",
                Sender = "test@company.com",
                Recipients = new List<string>() { "testRecipient@company.com" }
            };

            var result = await SendAsync(createEmailCommand);

            result.GetType().Should().BeAssignableTo<Guid>();
        }
    }
}
