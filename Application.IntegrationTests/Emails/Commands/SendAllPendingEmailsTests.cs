﻿using Application.Common.Interfaces;
using Application.Emails.Commands.SendAllPendingEmails;
using Domain.Enums;
using FluentAssertions;
using Infrastructure.Persistance.Configurations;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.IntegrationTests.Emails.Commands
{
    using static Testing;

    public class SendAllPendingEmailsTests : TestBase
    {
        [Test]
        public async Task ShouldReturnTrueWhenAllPendingEmailsAreSent()
        {
            using var scope = scopeFactory.CreateScope();
            var emailRepository = scope.ServiceProvider.GetRequiredService<IEmailRepository>();

            var pendingEmail = new Domain.Entities.Email()
            {
                Subject = "Test subject",
                Message = "Test Message",
                Sender = "test@company.com",
                Status = EmailStatus.Pending,
                Recipients = new List<string>() { "testRecipient@company.com" }
            };

            await emailRepository.Insert(pendingEmail);

            var createEmailCommand = new SendAllPendingEmailsCommand();

            var result = await SendAsync(createEmailCommand);

            result.Should().Be(true);
            var emails = await emailRepository.SelectAll();
            var areEmailsStatusCorrect = emails.All(email => email.Status == EmailStatus.Sent);
            areEmailsStatusCorrect.Should().Be(true);
        }
    }
}
