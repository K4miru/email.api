﻿using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Emails.Commands.UpdateEmail;
using Domain.Enums;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.IntegrationTests.Emails.Commands
{
    using static Testing;
    public class UpdateEmailTests : TestBase
    {
        [Test]
        public async Task ShouldUpdateEmailDetailsById()
        {
            using var scope = scopeFactory.CreateScope();
            var emailRepository = scope.ServiceProvider.GetRequiredService<IEmailRepository>();

            var newEmail = new Domain.Entities.Email()
            {
                Subject = "Test subject",
                Message = "Test Message",
                Sender = "test@company.com",
                Status = EmailStatus.Pending,
                Recipients = new List<string>() { "testRecipient@company.com" }
            };
            var dateNow = DateTime.UtcNow;
            Guid id = await emailRepository.Insert(newEmail);

            var query = new UpdateEmailCommand() 
            {
                Id = id,
                Subject = "Test subject 2",
                Message = "Test Message 2",
                Sender = "tes2t@company.com",
                Recipients = new List<string>() { "testRecipient@company.com", "testRecipient2@company.com" }
            };

            await SendAsync(query);

            var result = await emailRepository.Select(id);

            result.Subject.Should().Be(query.Subject);
            result.Sender.Should().Be(query.Sender);
            result.Message.Should().Be(query.Message);
            result.Status.Should().Be(newEmail.Status);
            result.Recipients.Should().BeEquivalentTo(query.Recipients);
            result.Created.Should().BeSameDateAs(dateNow);
            result.LastModified.Should().BeSameDateAs(dateNow);
        }

        [Test]
        public async Task ShouldThrowNotFoundExceptionOnNotExistingId()
        {
            Guid id = Guid.NewGuid();

            var query = new UpdateEmailCommand() { Id = id };

            Func<Task> action = async () => await SendAsync(query);

            action.Should()
                  .Throw<NotFoundException>()
                  .WithMessage($"Entity \"{nameof(Email)}\" ({id.ToString()}) was not found.");
        }
    }
}
