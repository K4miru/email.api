﻿using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Emails.Queries.GetEmailDetails;
using Domain.Enums;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.IntegrationTests.Emails.Queries
{
    using static Testing;
    public class GetEmailDetailsTests : TestBase
    {
        [Test]
        public async Task ShouldReturnEmailDetailsById()
        {
            using var scope = scopeFactory.CreateScope();
            var emailRepository = scope.ServiceProvider.GetRequiredService<IEmailRepository>();

            var newEmail = new Domain.Entities.Email()
            {
                Subject = "Test subject",
                Message = "Test Message",
                Sender = "test@company.com",
                Status = EmailStatus.Pending,
                Recipients = new List<string>() { "testRecipient@company.com" }
            };
            var dateNow = DateTime.UtcNow;
            Guid id = await emailRepository.Insert(newEmail);

            var query = new GetEmailDetailsQuery() { Id = id };

            var result = await SendAsync(query);

            result.Subject.Should().Be(newEmail.Subject);
            result.Sender.Should().Be(newEmail.Sender);
            result.Message.Should().Be(newEmail.Message);
            result.Status.Should().Be(newEmail.Status);
            result.Recipients.Should().BeEquivalentTo(newEmail.Recipients);
            result.Created.Should().BeSameDateAs(dateNow);
            result.LastModified.Should().BeSameDateAs(dateNow);
        }

        [Test]
        public async Task ShouldThrowNotFoundExceptionOnNotExistingId()
        {
            Guid id = Guid.NewGuid();

            var query = new GetEmailDetailsQuery() { Id = id };

            Func<Task> action = async () => await SendAsync(query);

            action.Should()
                  .Throw<NotFoundException>()
                  .WithMessage($"Entity \"{nameof(Email)}\" ({id.ToString()}) was not found.");
        }
    }
}
