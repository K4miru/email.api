﻿using Application.Common.Interfaces;
using Application.Emails.Queries.GetEmails;
using Domain.Enums;
using NUnit.Framework;
using System.Threading.Tasks;
using System.Collections.Generic;
using FluentAssertions;
using Infrastructure.Persistance.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Application.IntegrationTests.Emails.Queries
{
    using static Testing;
    public class GetEmailsTests : TestBase
    {
        [Test]
        public async Task ShouldReturnAllEmails()
        {
            using var scope = scopeFactory.CreateScope();
            var emailRepository = scope.ServiceProvider.GetRequiredService<IEmailRepository>();

            var pendingEmail = new Domain.Entities.Email()
            {
                Subject = "Test subject",
                Message = "Test Message",
                Sender = "test@company.com",
                Status = EmailStatus.Pending,
                Recipients = new List<string>() { "testRecipient@company.com" }
            };
            var sentEmail = new Domain.Entities.Email()
            {
                Subject = "Test subject",
                Message = "Test Message",
                Sender = "test@company.com",
                Status = EmailStatus.Sent,
                Recipients = new List<string>() { "testRecipient@company.com" }
            };

            await emailRepository.Insert(pendingEmail);
            await emailRepository.Insert(sentEmail);

            var query = new GetEmailsQuery();

            var result = await SendAsync(query);

            result.Should().HaveCount(2);
        }
    }
}
