﻿using Application.Common.Interfaces;
using Email.Api;
using Infrastructure.Persistance.Configurations;
using Infrastructure.Persistance.Repositories;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.IntegrationTests
{
    [SetUpFixture]
    public class Testing
    {
        private static IConfigurationRoot configuration;
        public static IServiceScopeFactory scopeFactory;
        public static IMongoDatabase Database;

        [OneTimeSetUp]
        public void RunBeforeAnyTests()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true)
                .AddEnvironmentVariables();

            configuration = builder.Build();

            var startup = new Startup(configuration);

            var services = new ServiceCollection();

            services.AddSingleton(Mock.Of<IWebHostEnvironment>(w =>
                w.EnvironmentName == "Development" &&
                w.ApplicationName == "Email.Api"));

            services.AddSingleton<IConfiguration>(configuration);
            services.AddScoped<IEmailRepository, EmailRepository>();

            startup.ConfigureServices(services);

            var mockedEmailSenderService = Mock.Of<IEmailSenderService>();
            services.Remove(services.FirstOrDefault(x => x.ServiceType == typeof(IEmailSenderService)));
            services.AddSingleton(mockedEmailSenderService);

            scopeFactory = services.BuildServiceProvider().GetService<IServiceScopeFactory>();

            using var scope = scopeFactory.CreateScope();
            IMongoClient client = scope.ServiceProvider.GetService<IMongoClient>();

            var databaseName = MongoUrl.Create(configuration.GetConnectionString("DefaultConnection")).DatabaseName;
            Database = client.GetDatabase(databaseName);
        }

        public static async Task<TResponse> SendAsync<TResponse>(IRequest<TResponse> request)
        {
            using var scope = scopeFactory.CreateScope();

            var mediator = scope.ServiceProvider.GetService<IMediator>();

            return await mediator.Send(request);
        }

        public static async Task ResetState()
        {
            await Database.DropCollectionAsync(EmailConfiguration.CollectionName);
        }

        [OneTimeTearDown]
        public void RunAfterAnyTests()
        {
        }
    }
}
