﻿using System;

namespace Application.Common.Exceptions
{
    public class EmailNotSentException : Exception
    {
        public EmailNotSentException()
            : base()
        {
        }

        public EmailNotSentException(string message)
            : base(message)
        {
        }

        public EmailNotSentException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
}
}
