﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Common.Interfaces
{
    public interface IEmailRepository
    {
        Task<IEnumerable<Email>> SelectAll();

        Task<Guid> Insert(Email email);
        Task Update(Email email);
        Task<Email> Select(Guid id);
    }
}
