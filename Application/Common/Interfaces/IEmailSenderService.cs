﻿using Domain.Entities;
using System.Threading.Tasks;

namespace Application.Common.Interfaces
{
    public interface IEmailSenderService
    {
        void Send(Email email);
    }
}
