﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Emails.Commands.CreateEmail
{
    public class CreateEmailCommand : IRequest<Guid>
    {
        public string Subject { get; set; }
        public string Message { get; set; }
        public string Sender { get; set; }
        public IEnumerable<string> Recipients { get; set; }
    }
}
