﻿using Application.Common.Interfaces;
using Domain.Entities;
using Domain.Enums;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Emails.Commands.CreateEmail
{
    public class CreateEmailCommandHandler : IRequestHandler<CreateEmailCommand, Guid>
    {
        private readonly IEmailRepository emailRepository;

        public CreateEmailCommandHandler(IEmailRepository emailRepository) 
        {
            this.emailRepository = emailRepository;
        }

        public async Task<Guid> Handle(CreateEmailCommand request, CancellationToken cancellationToken)
        {
            var email = new Email()
            {
                Subject = request.Subject,
                Message = request.Message,
                Sender = request.Sender,
                Recipients = request.Recipients,
                Status = EmailStatus.Pending,
            };
            var id = await emailRepository.Insert(email);
            return id;
        }
    }
}
