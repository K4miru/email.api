﻿using MediatR;

namespace Application.Emails.Commands.SendAllPendingEmails
{
    public class SendAllPendingEmailsCommand : IRequest<bool>
    {
    }
}
