﻿using Application.Common.Interfaces;
using Domain.Enums;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Emails.Commands.SendAllPendingEmails
{
    public class SendAllPendingEmailsCommandHandler : IRequestHandler<SendAllPendingEmailsCommand, bool>
    {
        private readonly IEmailRepository emailRepository;
        private readonly IEmailSenderService emailSenderService;
        public SendAllPendingEmailsCommandHandler(
            IEmailRepository emailRepository,
            IEmailSenderService emailSender)
        {
            this.emailRepository = emailRepository;
            this.emailSenderService = emailSender;
        }

        public async Task<bool> Handle(SendAllPendingEmailsCommand request, CancellationToken cancellationToken)
        {
            var emails = await emailRepository.SelectAll();
            var pendingEmails = emails.Where(email => email.Status == EmailStatus.Pending);
            foreach (var email in pendingEmails)
            {
                emailSenderService.Send(email);
                email.Status = EmailStatus.Sent;
                await emailRepository.Update(email);
            }
            return true;
        }
    }
}
