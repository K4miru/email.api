﻿using MediatR;
using System;
using System.Collections.Generic;

namespace Application.Emails.Commands.UpdateEmail
{
    public class UpdateEmailCommand : IRequest
    {
        public Guid Id { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string Sender { get; set; }
        public IEnumerable<string> Recipients { get; set; }
    }
}
