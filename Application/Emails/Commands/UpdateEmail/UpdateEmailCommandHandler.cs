﻿using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Emails.Commands.UpdateEmail
{
    public class UpdateEmailCommandHandler : IRequestHandler<UpdateEmailCommand>
    {
        private readonly IEmailRepository emailRepository;

        public UpdateEmailCommandHandler(IEmailRepository emailRepository)
        {
            this.emailRepository = emailRepository;
        }

        public async Task<Unit> Handle(UpdateEmailCommand request, CancellationToken cancellationToken)
        {
            var email = await emailRepository.Select(request.Id);

            if (email == null)
            {
                throw new NotFoundException(nameof(Email), request.Id.ToString());
            }

            email.Subject = request.Subject;
            email.Message = request.Message;
            email.Sender = request.Sender;
            email.Recipients = request.Recipients;

            await emailRepository.Update(email);
            return Unit.Value;
        }
    }
}
