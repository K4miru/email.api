﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Emails.Commands.UpdateEmail
{
    public class UpdateEmailCommandValidator : AbstractValidator<UpdateEmailCommand>
    {
        public UpdateEmailCommandValidator()
        {
            RuleFor(v => v.Sender)
                .NotNull()
                .Must(IsValidEmail).WithMessage("This value does not represent email format");

            RuleForEach(v => v.Recipients)
                .NotNull()
                .Must(IsValidEmail).WithMessage("This value does not represent email format");
        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}
