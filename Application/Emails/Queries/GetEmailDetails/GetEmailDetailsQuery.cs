﻿using MediatR;
using System;

namespace Application.Emails.Queries.GetEmailDetails
{
    public class GetEmailDetailsQuery : IRequest<EmailDetailsDto>
    {
        public Guid Id { get; set; }
    }
}
