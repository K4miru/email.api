﻿using Application.Common.Exceptions;
using Application.Common.Interfaces;
using AutoMapper;
using Domain.Entities;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Emails.Queries.GetEmailDetails
{
    public class GetEmailDetailsQueryHandler : IRequestHandler<GetEmailDetailsQuery, EmailDetailsDto>
    {
        private readonly IEmailRepository emailRepository;
        private readonly IMapper mapper;

        public GetEmailDetailsQueryHandler(
            IEmailRepository emailRepository,
            IMapper mapper)
        {
            this.emailRepository = emailRepository;
            this.mapper = mapper;
        }

        public async Task<EmailDetailsDto> Handle(GetEmailDetailsQuery request, CancellationToken cancellationToken)
        {
            var email = await emailRepository.Select(request.Id);
            if (email == null)
            {
                throw new NotFoundException(nameof(Email), request.Id.ToString());
            }
            var emailDetailDto = mapper.Map<EmailDetailsDto>(email);
            return emailDetailDto;
        }
    }
}
