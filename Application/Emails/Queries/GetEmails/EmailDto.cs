﻿using Application.Common.Mappings;
using Domain.Entities;
using System;

namespace Application.Emails.Queries.GetEmails
{
    public class EmailDto : IMapFrom<Email>
    {
        public Guid Id { get; set; }
        public string Subject { get; set; }
        public string Sender { get; set; }
    }
}
