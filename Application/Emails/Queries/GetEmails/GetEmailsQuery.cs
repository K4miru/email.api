﻿using MediatR;
using System.Collections.Generic;

namespace Application.Emails.Queries.GetEmails
{
    public class GetEmailsQuery : IRequest<IEnumerable<EmailDto>>
    {
    }
}
