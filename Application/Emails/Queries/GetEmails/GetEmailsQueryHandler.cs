﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using AutoMapper;
using MediatR;

namespace Application.Emails.Queries.GetEmails
{
    public class GetEmailsQueryHandler : IRequestHandler<GetEmailsQuery, IEnumerable<EmailDto>>
    {
        private readonly IEmailRepository emailRepository;
        private readonly IMapper mapper;

        public GetEmailsQueryHandler(
            IEmailRepository emailRepository,
            IMapper mapper)
        {
            this.emailRepository = emailRepository;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<EmailDto>> Handle(GetEmailsQuery request, CancellationToken cancellationToken)
        {
            var emails = await emailRepository.SelectAll();
            var emailDtos = mapper.Map<IEnumerable<EmailDto>>(emails);
            return emailDtos;
        }
    }
}
