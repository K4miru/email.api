﻿using System;
using System.Collections.Generic;
using Domain.Enums;

namespace Domain.Entities
{
    public class Email
    {
        public Guid Id { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string Sender { get; set; }
        public IEnumerable<string> Recipients { get; set; }
        public EmailStatus Status { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastModified { get; set; }
    }
}
