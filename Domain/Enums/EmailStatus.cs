﻿namespace Domain.Enums
{
    public enum EmailStatus
    {
        Pending,
        Sent
    }
}
