﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Emails.Commands.CreateEmail;
using Application.Emails.Commands.SendAllPendingEmails;
using Application.Emails.Queries.GetEmails;
using Application.Emails.Queries.GetEmailDetails;
using Microsoft.AspNetCore.Mvc;
using Application.Emails.Commands.UpdateEmail;

namespace Email.Api.Controllers
{
    public class EmailsController : BaseController
    {
        [HttpGet]
        public async Task<IEnumerable<EmailDto>> GetEmails()
        {
            return await Mediator.Send(new GetEmailsQuery());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<EmailDetailsDto>> GetEmail(Guid id)
        {
            return await Mediator.Send(new GetEmailDetailsQuery() { Id = id});
        }

        [HttpPost]
        public async Task<ActionResult<Guid>> CreateEmail(CreateEmailCommand createEmailCommand)
        {
            return await Mediator.Send(createEmailCommand);
        }

        [HttpPost("pending/send")]
        public async Task<ActionResult<bool>> SendAllPendingEmails(SendAllPendingEmailsCommand sendAllPendingEmailsCommand)
        {
            return await Mediator.Send(sendAllPendingEmailsCommand);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateEmail(Guid id, UpdateEmailCommand updateEmailCommand)
        {
            if (id != updateEmailCommand.Id)
            {
                return BadRequest();
            }

            await Mediator.Send(updateEmailCommand);
            return NoContent();
        }
    }
}
