﻿using Application.Common.Interfaces;
using Infrastructure.Persistance.Configurations;
using Infrastructure.Persistance.Repositories;
using Infrastructure.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;

namespace Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            EmailDbConfiguration.Configure();
            services.AddSingleton<IMongoClient>(new MongoClient(configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped<IEmailRepository, EmailRepository>();
            services.AddScoped<IEmailSenderService, EmailSenderService>();
            return services;
        }
    }
}
