﻿using Domain.Entities;
using MongoDB.Bson.Serialization;

namespace Infrastructure.Persistance.Configurations
{
    public static class EmailConfiguration
    {
        public static string CollectionName => "emails";

        public static void Configure()
        {
            BsonClassMap.RegisterClassMap<Email>(cm =>
            {
                cm.AutoMap();
                cm.MapIdMember(c => c.Id);
            });
        }
    }
}
