﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistance.Configurations
{
    public static class EmailDbConfiguration
    {
        public static void Configure()
        {
            EmailConfiguration.Configure();
        }
    }
}
