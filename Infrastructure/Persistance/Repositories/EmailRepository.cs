﻿using Application.Common.Interfaces;
using Domain.Entities;
using Infrastructure.Persistance.Configurations;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.Persistance.Repositories
{
    public class EmailRepository : IEmailRepository
    {
        private IMongoDatabase database;

        private IMongoCollection<Email> collection;

        public EmailRepository(IMongoClient client, IConfiguration configuration)
        {
            var databaseName = MongoUrl.Create(configuration.GetConnectionString("DefaultConnection")).DatabaseName;
            database = client.GetDatabase(databaseName);
            collection = database.GetCollection<Email>(EmailConfiguration.CollectionName);
        }

        public async Task<IEnumerable<Email>> SelectAll()
        {
            return await collection.Find(new BsonDocument()).ToListAsync();
        }

        public async Task<Guid> Insert(Email email)
        {
            email.Id = Guid.NewGuid();
            email.Created = DateTime.UtcNow;
            email.LastModified = DateTime.UtcNow;
            await collection.InsertOneAsync(email);
            return email.Id;
        }

        public async Task Update(Email email)
        {
            email.LastModified = DateTime.UtcNow;
            var builder = Builders<Email>.Filter;
            var filter = builder.Eq(email => email.Id, email.Id);
            var updateEmail = Builders<Email>.Update.Set(s => s.Status, email.Status)
                                                    .Set(s => s.LastModified, email.LastModified)
                                                    .Set(s => s.Subject, email.Subject)
                                                    .Set(s => s.Message, email.Message)
                                                    .Set(s => s.Sender, email.Sender)
                                                    .Set(s => s.Recipients, email.Recipients);

            await collection.UpdateOneAsync(filter, updateEmail);
        }

        public async Task<Email> Select(Guid id)
        {
            var filter = Builders<Email>.Filter.Eq(email => email.Id, id);
            return await collection.Find(filter).FirstOrDefaultAsync();
        }
    }
}
