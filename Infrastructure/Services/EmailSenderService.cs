﻿using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Net;
using System.Net.Mail;

namespace Infrastructure.Services
{
    public class EmailSenderService : IEmailSenderService
    {
        private readonly string host;
        private readonly int port;
        private readonly string username;
        private readonly string password;

        public EmailSenderService(IConfiguration configuration)
        {
            var smtpConfiguration = configuration.GetSection("SmtpConfiguration");
            this.host = smtpConfiguration["host"];
            this.port = Int32.Parse(smtpConfiguration["port"]);
            this.username = smtpConfiguration["username"];
            this.password = smtpConfiguration["password"];
        }

        public void Send(Email email)
        {
            try
            {
                var recipients = string.Join(",", email.Recipients);

                using (SmtpClient smtpClient = new SmtpClient(host, port))
                {
                    MailMessage mailMessage = new MailMessage(email.Sender, recipients, email.Subject, email.Message);
                    NetworkCredential credentials = new NetworkCredential(username, password);
                    smtpClient.Credentials = credentials;
                    smtpClient.Send(mailMessage);
                }
            }
            catch (Exception ex)
            {
                throw new EmailNotSentException($"Email with {email.Id} was not send by host {host}", ex);
            }
        }
    }
}
